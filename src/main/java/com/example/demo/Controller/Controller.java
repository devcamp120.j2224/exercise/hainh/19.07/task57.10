package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Animal;
import com.example.demo.Model.Duck;
import com.example.demo.Model.Fish;

@RestController
public class Controller {
    
    @CrossOrigin
    @GetMapping("listAnimal")
    public ArrayList<Animal>listAnimal(){
        ArrayList<Animal> animals = new ArrayList<Animal>();

        Animal duck = new Duck();
        duck.setAge(2);
        duck.setGender("male");
        ((Duck)duck).setBreakColor("red");
        duck.mate();
        duck.isMamal();
        
        Duck duck2 = new Duck(3 , "female" , "yellow");
        duck2.mate();
        duck2.Swim();
        duck2.Quack();
        duck2.isMamal();

        Fish fish = new Fish(1 , "green",  4 , true);
        fish.mate();
        fish.isMamal();
        fish.swim();

        animals.add(duck);
        animals.add(duck2);
        animals.add(fish);

        return animals;
        
    }
}
