package com.example.demo.Model;

public class Fish extends Animal {
    private int size ;
    private boolean canEat ;
    
    public Fish(int size, boolean canEat) {
        this.size = size;
        this.canEat = canEat;
    }

    public Fish(int age, String gender, int size, boolean canEat) {
        super(age, gender);
        this.size = size;
        this.canEat = canEat;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isCanEat() {
        return canEat;
    }

    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }

    @Override
    public void isMamal() {
       System.out.println("Fish is not mamal"); 
    }
    
    public void swim(){
        System.out.println("Fish swim");
    }
}
