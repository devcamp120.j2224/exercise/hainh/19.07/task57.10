package com.example.demo.Model;

public class Zebra extends Animal {
    private boolean is_wild ;

    
    public boolean isIs_wild() {
        return is_wild;
    }

    public void setIs_wild(boolean is_wild) {
        this.is_wild = is_wild;
    }

    public Zebra(boolean is_wild) {
        this.is_wild = is_wild;
    }

    public Zebra(int age, String gender, boolean is_wild) {
        super(age, gender);
        this.is_wild = is_wild;
    }

    @Override
    public void isMamal() {
        // TODO Auto-generated method stub
        
    }
    
    public void run(){
        System.out.println("Zebra Running");
    }

}
