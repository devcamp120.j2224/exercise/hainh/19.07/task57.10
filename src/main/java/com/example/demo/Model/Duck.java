package com.example.demo.Model;

public class Duck extends Animal {
    private String breakColor ;

    public Duck() {}

    public Duck(String breakColor) {
        super();
        this.breakColor = breakColor;
    }

    public Duck(int age, String gender, String breakColor) {
        super(age, gender);
        this.breakColor = breakColor;
    }

    public String getBreakColor() {
        return breakColor;
    }

    public void setBreakColor(String breakColor) {
        this.breakColor = breakColor;
    }

    @Override
    public void isMamal() {
        System.out.println("Duck is mamal");
    }
    
    public void Swim(){
        System.out.println("Duck is swim");
    }

    public void Quack(){
        System.out.println("Duck is quack");
    }

   
}
